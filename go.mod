module git.openprivacy.ca/openprivacy/lockbox

go 1.13

require (
	git.openprivacy.ca/openprivacy/log v1.0.0
	github.com/therecipe/qt v0.0.0-20191101232336-18864661ae4f
    github.com/therecipe/qt/internal/binding/files/docs/5.13.0 v0.0.0-20191002095216-73192f6811d0 // indirect; Required - do not delete or `go tidy` away
	golang.org/x/crypto v0.0.0-20200320181102-891825fb96df
)
