import QtQuick 2.7 //ApplicationWindow
import QtQuick.Controls 2.1 //Dialog
import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4

ApplicationWindow {
    id: root
    visible: true
    title: "Open Privacy LockBox"
    minimumWidth: 640
    minimumHeight: 640

    property string keyfilegentxt
    property string outfiletxt
    property string keyfiletxt
    property string inputfiletxt


	TabView {
		anchors.fill: parent

		Tab {
			title: "Instructions"

			Column {
				anchors.fill: parent
				padding: 10

				Label {
					width: parent.width
					wrapMode: Text.WordWrap
					rightPadding: 10
					text: "This application works in two steps:\n\n1. Use the Generate Keys tab to create files named 'key.public' and 'key.private'. These are encryption and decryption keys, respectively. The public key should be sent to whoever set up the partner web app.  The private key should be backed up to a USB stick or somewhere else trustworthy for backup, but you should be careful not to send it over the internet!\n\n2. Once you have downloaded the submissions.dat file from the web app's admin panel, use the Decrypt tab and the private key file from earlier to decrypt the file.  It will output a CSV (spreadsheet) file that can be opened in office programs."
				}
			}
		}

		Tab {
			title: "Generate Keys"

			Column {
				padding: 10
				width: parent.width

				Row {
					width:parent.width
					padding:10
					Label {
						text: "Generate New Encryption Keys"
						font.pixelSize: 22
					}
				}
				Row {
					width:parent.width
					padding:10
					TextField {
						width:parent.width * 0.50
						id:keyFileGenLabel
						placeholderText: "Folder to save keys into"
						text: root.keyfilegentxt
					}
					Button {
						Layout.alignment: Qt.AlignRight
						text: "Browse..."
						onClicked: function() {
							keyFileCreateDialog.visible = true
						}
					}
				}

				Row {
					width:parent.width
					padding: 10
					Button {
						text: "Generate!"
						onClicked: function() {
							lockbox.generateKey(root.keyfilegentxt)
						}
					}
				}

			}
		}

		Tab {
			title: "Decrypt"

		   Column {
				anchors.fill: parent
				padding:10

				Row {
					width:parent.width
					padding:10

					Label {
						text: "Decrypt submissions"
						font.pixelSize: 22
					}
				}

				Row {
					width:parent.width
					padding:10
					TextField {
						width:parent.width * 0.75
						id:inputFileLabel
						placeholderText: "Encrypted submissions file (.dat)"
						text: root.inputfiletxt
					}
					Button {
						Layout.alignment: Qt.AlignRight
						text: "Browse..."
						onClicked: fileDialog.visible = true
					}
				}
				Row {
					width:parent.width
					padding:10
					TextField {
						id:outputFileLabel
						width:parent.width * 0.75
						placeholderText: "Location for decrypted file (.csv)"
						text: root.outfiletxt
					}
					Button {
						Layout.alignment: Qt.AlignRight
						text: "Browse..."
						onClicked: outputFileDialog.visible = true
					}
				}
				Row {
					width:parent.width
					padding:10
					TextField {
						width:parent.width * 0.75
						id:keyFileLabel
						placeholderText: "Decryption key file (.private)"
						text: root.keyfiletxt
					}
					Button {
						Layout.alignment: Qt.AlignRight
						text: "Browse..."
						onClicked: keyFileDialog.visible = true
					}
				}
				Row {
					width:parent.width
					padding:10
					Button {
						text: "Decrypt!"
						onClicked: function() {
							lockbox.decryptFile(root.inputfiletxt,root.outfiletxt,root.keyfiletxt)
						}
					}
				}
			}
		}
	}

    FileDialog {
        id: outputFileDialog
        title: "Please choose a file save to"
        nameFilters: [ "CSV File (*.csv)", "All files (*)" ]
        folder: shortcuts.home
        selectFolder: false
        selectMultiple: false
        selectExisting:false
        onAccepted: {
            console.log("You chose: " + outputFileDialog.fileUrls)
            root.outfiletxt =  outputFileDialog.fileUrls[0]
        }
        onRejected: {
            console.log("Canceled")
        }
        Component.onCompleted: visible = false


    }


    FileDialog {
        id: keyFileDialog
        title: "Please choose an encryption key file"
        nameFilters: [ "Key File (*.private)", "All files (*)" ]
        folder: shortcuts.home
        selectFolder: false
        selectMultiple: false
        onAccepted: {
            console.log("You chose: " + keyFileDialog.fileUrls)
            root.keyfiletxt =  keyFileDialog.fileUrls[0]
        }
        onRejected: {
            console.log("Canceled")
        }
        Component.onCompleted: visible = false
    }

    FileDialog {
        id: keyFileCreateDialog
        title: "Please choose where to save the encryption key file"
        nameFilters: [ "Key File (*.private)", "All files (*)" ]
        folder: shortcuts.home
        selectFolder: true
        selectMultiple: false
        onAccepted: {
            console.log("You chose: " + keyFileCreateDialog.fileUrls)
            root.keyfilegentxt =  keyFileCreateDialog.fileUrls[0]
        }
        onRejected: {
            console.log("Canceled")
        }
        Component.onCompleted: visible = false
    }

    Connections { // POPUPS ARE INVOKED BY GO FUNCS
            target: lockbox

            onDecrypted: function(status, message) {
                if (status) {
                    messageDialog.icon = StandardIcon.Information
                    messageDialog.title = "Decrypted Successfully"
                } else {
                    messageDialog.icon = StandardIcon.Critical
                    messageDialog.title = "Error Decrypting"
                }

                messageDialog.text = message
                messageDialog.open()
            }

            onSaved: function(status, message) {
                if (status) {
                    messageDialog.icon = StandardIcon.Information
                    messageDialog.title = "Encryption Keys Generating Successfully"
                } else {
                    messageDialog.icon = StandardIcon.Critical
                    messageDialog.title = "Error Generating Encryption Keys"
                }

                messageDialog.text = message
                messageDialog.open()
            }
    }


    FileDialog {
        id: fileDialog
        title: "Please choose a file to decrypt"
        nameFilters: [ "Dat File (*.dat)", "All files (*)" ]
        folder: shortcuts.home
        selectFolder: false
        selectMultiple: false
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
            root.inputfiletxt =  fileDialog.fileUrls[0]
        }
        onRejected: {
            console.log("Canceled")
        }
        Component.onCompleted: visible = false
    }


    MessageDialog {
        id: messageDialog
        title: "May I have your attention please"
        text: "It's so cool that you are using Qt Quick."
        onAccepted: {
            messageDialog.close()
        }
        Component.onCompleted: visible = false
    }

}
