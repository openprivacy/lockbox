![](./lockbox.png)

Host a submission form on an untrusted or shared host by using public-key encryption! This repository hosts the source code for the Lockbox app, which works together with the [Lockbox web app](https://git.openprivacy.ca/openprivacy/lockbox-web).

Lockbox is a barebones form submission app intended to be easily adaptable for different needs. It works by capturing all submitted form data and encrypting it with a (libsodium) public key before saving it.  Saved data can only be read by decrypting it with your unique private key, which can be kept offline and protected however you like.

## Installation

* Use the Lockbox app or the `cmd/genkeys.php` script to generate `key.public` and `key.private` files
* Only people with the `key.private` file can decrypt submissions -- make a backup copy of it and keep it somewhere safe! If you lose it, you won't be able to recover any submissions you haven't decrypted yet.
* Upload the [web app files](https://git.openprivacy.ca/openprivacy/lockbox-web) and your generated `key.public` file onto a webserver that supports PHP
* Configure the form by editing `php/form.inc.php` and `php/config.inc.php`
* Rename admin.php to something unpredictable if you would like to use it

## Usage

* Submissions can only be decrypted using the `key.private` you generated earlier.
* Download the encrypted `submissions.dat` file either directly from your server or by using the renamed `admin.php` script.
* Use the Lockbox app or `cmd/decrypt.php` to decrypt submissions. It will output a CSV file that can be opened in any spreadsheet editor (such as Microsoft Excel or LibreOffice Calc).

## Making HTML forms

* We are working on attaching an HTML form generator. For now, you can write HTML by hand or use any form editor you prefer that is capable of outputting HTML.
* The submission script will capture all form fields submitted to it.
* Field ordering is not preserved by default. If you would like spreadsheet columns to appear in a certain order, you can give your form fields a number and an underscore, for example `01_name`, `02_address`, `03_phone` etc. The Lockbox app will remove the numeric prefix when creating the spreadsheet.
* Do not name a form field `email` as it is used for detecting bots.
* Submission time, submission number, and submitter IP address are all added automatically.

## Threat Model

* Lockbox's encryption is intended to protect against attackers that gain read-only access to the webserver where the form is hosted. Attackers that get access to encrypted data cannot decrypt it.
* Lockbox does not and cannot protect against attackers that can modify the web app. The app can be modified so that submissions received *after* the compromise are intercepted before they are encrypted. (Submissions received *before* such a compromise would remain safely encrypted.)